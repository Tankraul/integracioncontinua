import { render, screen } from '@testing-library/react';
import App from './App';

test('test de prueba', () => {
  render(<App />);
  const linkElement = screen.getByText(/Editar esto/i);
  expect(linkElement).toBeInTheDocument();
});
